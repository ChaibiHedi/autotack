import matplotlib.pyplot as plt
import pandas as pd
from mpl_toolkits.basemap import Basemap
import numpy as np

df = pd.read_csv("Data/test_data.csv")
df = df.dropna()

coordinates_df = df[["Latitude", "Longitude", "Tacking"]]
coordinates_df.Tacking = coordinates_df.Tacking.astype(int)

fig = plt.figure(figsize=(12, 12), dpi=100)

m = Basemap(projection='cyl', resolution="h",
            llcrnrlon=-70, llcrnrlat=0, urcrnrlon=-50, urcrnrlat=25,
            epsg=2002)

m.arcgisimage(service='ESRI_Imagery_World_2D', xpixels=2000, verbose= True)

latitude = df.Latitude.values
longitude = df.Longitude.values
categories = list(df.Tacking.values)

color_map = {0: "red", 1:"yellow"}

# x, y = m(latitude,longitude)
x, y = m(longitude, latitude)

plt.scatter(x, y, 10, marker='o', c=list(map(color_map.get, categories)))

plt.savefig("map.png", dpi=100)
plt.show()

