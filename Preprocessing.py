import math

import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def to_octant(angle):
    if 0 <= angle < 45:

        return "1"

    elif 45 <= angle < 90:

        return "2"

    elif 90 <= angle < 135:

        return "3"

    elif 135 <= angle < 180:

        return "4"

    elif 180 <= angle < 225:

        return "5"

    elif 225 <= angle < 270:

        return "6"

    elif 270 <= angle < 315:

        return "7"

    else:

        return "8"


def load_data(step_target=False, normalise=True, angle_method="trigo", resampling_freq=10):
    df = pd.read_csv("Data/test_data.csv")

    df["DateTime"] = pd.to_datetime(df["DateTime"])
    df = df.set_index("DateTime")

    sensor_stop_rows = df.loc["2019-04-14 16:56:04":"2019-04-14 16:58:40"]
    df.drop(sensor_stop_rows.index, inplace=True)
    df.fillna(method="ffill", inplace=True)

    df["Tacking"] = df["Tacking"].astype(int)
    df["ModePilote"] = df["ModePilote"].astype(int)

    # tacking_times = []
    #
    # counter = 0
    #
    # for i in range(len(df)):
    #
    #     if df.Tacking[i] == 1:
    #
    #         counter += 1
    #
    #     else:
    #
    #         if df.Tacking[i - 1] == 1:
    #             tacking_times.append(counter)
    #             counter = 0

    degrees_columns = ["CurrentDir", "TWA", "AWA", "Roll", "Pitch", "HeadingMag", "HoG", "HeadingTrue",
                       "RudderAng", "Leeway", "TWD"]

    num_columns = list(set(df.columns) - set(degrees_columns))

    resampled_df = df.resample(str(resampling_freq) + "S").median()
    resampled_df.dropna(inplace=True)

    resampled_df["NewTack"] = 0

    for i in range(1, len(resampled_df)):

        if resampled_df["Tacking"].iloc[i - 1] == 0 and resampled_df["Tacking"].iloc[i] == 1:
            resampled_df["NewTack"].iloc[i] = 1

    if angle_method == "cos_sin":

        resampled_df[degrees_columns] = resampled_df[degrees_columns].apply(
            lambda x: [math.radians(instance) for instance in x])

        for angular_feature in degrees_columns:
            cos_feature = np.cos(resampled_df[angular_feature])
            sin_feature = np.sin(resampled_df[angular_feature])

            cos_sin = pd.concat(
                [cos_feature.rename("cos_" + angular_feature), sin_feature.rename("sin_" + angular_feature)],
                axis=1)

            resampled_df = pd.concat([cos_sin, resampled_df], axis=1)

        resampled_df.drop(degrees_columns, axis=1, inplace=True)

    elif angle_method == "octant":

        for angular_feature in degrees_columns:
            resampled_df[angular_feature] = resampled_df[angular_feature].map(to_octant)

        resampled_df = pd.get_dummies(data=resampled_df, columns=degrees_columns)

    resampled_df.drop(["Latitude", "Longitude"], axis=1, inplace=True)

    if normalise:
        new_num_columns = list(set(resampled_df.columns).intersection(set(num_columns)))

        minmax_scaler = MinMaxScaler()

        resampled_df[new_num_columns] = minmax_scaler.fit_transform(resampled_df[new_num_columns])

    if step_target:

        resampled_df.drop("Tacking", axis=1, inplace=True)

        resampled_df = resampled_df[[c for c in resampled_df.columns if c not in ['NewTack']]
                                    + ['NewTack']]

    else:

        resampled_df.drop("NewTack", axis=1, inplace=True)

        resampled_df = resampled_df[[c for c in resampled_df.columns if c not in ['Tacking']]
                                    + ['Tacking']]

    return resampled_df
