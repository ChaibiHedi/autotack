import numpy as np
import pandas as pd
from keras.layers import Dense, LSTM, Dropout
from keras.models import Sequential
from matplotlib import pyplot as plt
from sklearn.metrics import classification_report
from tensorflow.keras.optimizers import SGD

from LSTM import smooth_predictions
from Preprocessing import load_data
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def prepare_dataset(dataset, n_steps, y_lag=0):
    X, y = list(), list()
    for i in range(len(dataset)):

        end_ix = i + n_steps
        if end_ix + y_lag > len(dataset):
            break

        seq_x, seq_y = dataset[i:end_ix, :-1], dataset[end_ix - 1 + y_lag, -1]
        X.append(seq_x)
        y.append(seq_y)
    return np.array(X), np.array(y)


def create_stacked_model(lstm_dim, n_steps, n_features, dropout=0, learning_rate=0.01):
    model = Sequential()

    if dropout:
        model.add(Dropout(dropout))

    lstm = LSTM(lstm_dim, input_shape=(n_steps, n_features), return_sequences=True)

    model.add(lstm)
    model.add(LSTM(round(lstm_dim / 2)))
    model.add(Dense(1, activation="sigmoid"))

    opt = SGD(learning_rate=learning_rate)

    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])

    return model


if __name__ == '__main__':

    df = load_data(resampling_freq=10, angle_method="octant")
    train, test = df["2019-04-14"], df["2019-04-15":]
    X_train, y_train = prepare_dataset(train.values, n_steps=180, y_lag=180)
    X_test, y_test = prepare_dataset(test.values, n_steps=180, y_lag=180)

    lstm_model = create_stacked_model(64, X_train.shape[1], X_train.shape[2], dropout=0.25)
    lstm_model.fit(X_train, y_train, epochs=15)

    predictions = (lstm_model.predict(X_test) > 0.95).astype("int32")

    predictions = smooth_predictions(predictions, 120)
    plt.plot(y_test)
    plt.plot(predictions)
    plt.savefig("30minuteslag.png", dpi=100)
    plt.show()
    print(classification_report(y_test, predictions))
